module.exports = {
  npm: {styles: {"normalize.css": ['normalize.css']}},
  files: {
    javascripts: {
      joinTo: {
        'vendor.js': /^(?!app)/,
        'app.js': /^app/
      }
    },
    stylesheets: {
      joinTo: {
        'vendor.css': /^node_modules/,
        'main.css': /^app/
      }
    }
  },

  plugins: {
    babel: {presets: ['es2015']}
  }
};
